from rgbmatrix import RGBMatrix, RGBMatrixOptions, graphics
from PIL import Image
import os
import time
import requests

class Display_32_64:

    # Fronts
    workingDir = os.path.dirname(os.path.abspath(__file__))

    # Configuration for the matrix
    options = RGBMatrixOptions()
    options.rows = 32
    options.cols = 64
    options.chain_length = 1
    options.parallel = 1
    options.hardware_mapping = 'adafruit-hat'
    options.gpio_slowdown = 5
    options.brightness = 100
    options.pwm_lsb_nanoseconds = 130
    options.limit_refresh_rate_hz = 500
    options.show_refresh_rate = 0
    options.pwm_dither_bits = 2
    matrix = RGBMatrix(options = options)

    # Configure canvas, fonts, text color.
    canvas  = matrix.CreateFrameCanvas()
    font1_3 = graphics.Font()
    font1_3.LoadFont(workingDir + "/7x13.bdf")
    font2_3 = graphics.Font()
    font2_3.LoadFont(workingDir + "/10x20.bdf")
    textColor = graphics.Color(255, 255, 0)
    textColor_Red = graphics.Color(255, 0, 0)
    textColor_Green = graphics.Color(0, 255, 0)
    textColor_Blue = graphics.Color(0, 0, 255)
    rgbArray = None

    def __init__(self, rgbArray):
        self.rgbArray = rgbArray

    def displayCountBasedStat(self, contentJson, oldContentJson):
        self.displayText(contentJson['statName'], 1, 12, True, False, self.textColor_Green, self.font1_3)
        xValue = int(((64-len(contentJson['statValue'])*10))/2)
        self.displayText(contentJson['statValue'], xValue, 28, False, True, self.textColor_Blue, self.font2_3)

        if(oldContentJson == None):
            return contentJson

        if(contentJson['statValue'].isdigit() and oldContentJson['statValue'].isdigit()):
            oldValue = int(oldContentJson['statValue'])
            newValue = int(contentJson['statValue'])
            while(oldValue < newValue):
                xValue = int(((64-len(str(oldValue))*10))/2)
                self.displayText(contentJson['statName'], 1, 12, True, False, self.textColor_Green, self.font1_3)
                self.displayText(str(oldValue), xValue, 28, False, True, self.textColor_Blue, self.font2_3)
                difference = newValue - oldValue
                if(difference > 1000):
                    oldValue += 111
                    time.sleep(0.025)
                elif(difference > 100):
                    oldValue += 11
                    time.sleep(0.025)
                else:
                    oldValue += 1
                    time.sleep(0.05)
            while(oldValue > newValue):
                xValue = int(((64-len(str(oldValue))*10))/2)
                self.displayText(contentJson['statName'], 1, 12, True, False, self.textColor_Green, self.font1_3)
                self.displayText(str(oldValue), xValue, 28, False, True, self.textColor_Blue, self.font2_3)
                difference = oldValue - newValue
                if(difference > 1000):
                    oldValue -= 111
                    time.sleep(0.025)
                elif(difference > 100):
                    oldValue -= 11
                    time.sleep(0.025)
                else:
                    oldValue -= 1
                    time.sleep(0.05)
        return contentJson

    def displayText(self, text, x, y, clear, swap, color, font):
        if clear:
            self.canvas.Clear()
            self.drawBackground()
        graphics.DrawText(self.canvas, font, x, y, color, text)
        if swap:
            self.canvas  = self.matrix.SwapOnVSync(self.canvas)

    def displayTextCenterX(self, text, y, clear, swap, color, font):
        if clear:
            self.canvas.Clear()
            self.drawBackground()
        graphics.DrawText(self.canvas, font, x, y, color, text)
        if swap:
            self.canvas  = self.matrix.SwapOnVSync(self.canvas)

    def drawBackground(self):
        for x in range(0, 64):
            self.canvas.SetPixel(x, 0, self.rgbArray[0], self.rgbArray[1], self.rgbArray[2])
            self.canvas.SetPixel(x, 32 - 1, self.rgbArray[0], self.rgbArray[1], self.rgbArray[2])
        for y in range(0, 32):
            self.canvas.SetPixel(0, y, self.rgbArray[0], self.rgbArray[1], self.rgbArray[2])
            self.canvas.SetPixel(64 - 1, y, self.rgbArray[0], self.rgbArray[1], self.rgbArray[2])

    def displayCode(self, code):
        self.displayText(code, 10, 20, True, True, self.textColor_Red, self.font1_3)

    def checkNetwork(self):
        failure = True
        while failure:
            try:
                requests.get("http://google.com")
                self.displayText("Connected!", 1, 20, True, True, self.textColor_Red, self.font1_3)
                time.sleep(2)
                failure = False
            except requests.exceptions.RequestException as e:
                self.displayText("Waiting", 0, 10, True, False, self.textColor_Red, self.font1_3)
                self.displayText("for", 0, 20, False, False, self.textColor_Red, self.font1_3)
                self.displayText("network", 0, 30, False, True, self.textColor_Red, self.font1_3)
                time.sleep(2)