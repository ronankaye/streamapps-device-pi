#!/usr/bin/env python3

# Root imports
import requests
import time
import os
import json
import time
import sys
import _thread
import array as arr
from pathlib import Path

# Matrix imports
from rgbmatrix import RGBMatrix, RGBMatrixOptions, graphics
from PIL import Image

# My Imports
from Display_32_64 import Display_32_64

# Files
workingDir = os.path.dirname(os.path.abspath(__file__))
displayDirPath = workingDir + '/.display'
deviceIdFilePath = displayDirPath + '/deviceId'

# The URL for the backend server that will be fulfilling our requests.
baseUrl = 'http://streamapps-backend.ronankaye.com'

# Endpoints needed.
requestCodeEndpoint = '/code/requestcode'
getDeviceContentEndpoint = '/device/get/content/' 

# Application Settings
pollRate = 0.5 # How often the backend is polled in seconds
hueShiftRate = 0.05 # How often to wait before shifting the hue in seconds

# Thread Variables
rgbArray = arr.array('i', [255, 0, 0])

def main():
    _thread.start_new_thread(hueShift_thread, (rgbArray, hueShiftRate))

    display = Display_32_64(rgbArray)

    display.checkNetwork()

    # Open a file that will store the deviceId. Create it if it doesnt exist.
    if not os.path.exists(os.path.dirname(deviceIdFilePath)):
        # Create path and files becuase they dont exist.
        Path(displayDirPath).mkdir(parents=True, exist_ok=True)
        Path(deviceIdFilePath).touch()
        deviceId = reserveAndDisplayShortCode(display)

    deviceIdFile = open(deviceIdFilePath, "r")
    deviceId = deviceIdFile.read()
    deviceIdFile.close()

    print("Entering main loop...")
    oldContentJson = None
    while(1==1):
        getContentResponse = requests.get(baseUrl + getDeviceContentEndpoint + deviceId)
        contentJson = getContentResponse.json()
        if(getContentResponse.status_code == 404):
            deviceId = reserveAndDisplayShortCode(display)
        # Update content on device
        oldContentJson = display.displayCountBasedStat(contentJson, oldContentJson)
        time.sleep(pollRate)

def reserveAndDisplayShortCode(display):
    print("reserveAndDisplayShortCode() called...")
    # Reserve a short code and device id.
    response = requests.get(baseUrl + requestCodeEndpoint)
    jsonResponse = response.json()
    shortCode = jsonResponse['shortCode']
    deviceId = jsonResponse['deviceId']
    # Log the short code and device id.
    print("reserveAndDisplayShortCode() : Short Code: " + shortCode)
    print("reserveAndDisplayShortCode() : Device Id: " + deviceId)
    # Save the deviceId to local storage.
    print("reserveAndDisplayShortCode() : Writing File...")
    deviceIdFile = open(deviceIdFilePath, "r+")
    deviceIdFile.write(deviceId)
    deviceIdFile.close()
    print("reserveAndDisplayShortCode() : File Written...")
    # Display the short code on the display panel.
    display.displayCode(shortCode)
    waitForUserToLinkDevice(deviceId)
    return deviceId

def waitForUserToLinkDevice(deviceId):
    print("waitForUserToLinkDevice() called...")
    # Loop until the device is linked.
    statusCode = 404
    while(statusCode == 404):
        print(statusCode)
        print("Waiting for link...")
        getContentResponse = requests.get(baseUrl + getDeviceContentEndpoint + deviceId)
        statusCode = getContentResponse.status_code
        time.sleep(pollRate)
    print(statusCode)

def hueShift_thread(rgbArray, delay):
    color1 = 0
    color2 = 1
    counter = 0
    while counter != 1000:
        while(rgbArray[color2] != 255):
            rgbArray[color2] += 1
            time.sleep(delay)
        while(rgbArray[color1] != 0):
            rgbArray[color1] -= 1
            time.sleep(delay)
        color1 = (color1+1)%3
        color2 = (color2+1)%3
        counter+=1

# Trigger main
if __name__=="__main__":
    main()